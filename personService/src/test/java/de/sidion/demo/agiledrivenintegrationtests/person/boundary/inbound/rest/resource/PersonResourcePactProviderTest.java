package de.sidion.demo.agiledrivenintegrationtests.person.boundary.inbound.rest.resource;


import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;
import au.com.dius.pact.provider.junitsupport.Provider;
import au.com.dius.pact.provider.junitsupport.State;
import au.com.dius.pact.provider.junitsupport.loader.PactBroker;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

@PactBroker
@Provider("PersonProvider")
@QuarkusTest
public class PersonResourcePactProviderTest {

    @BeforeEach
    void setup(final PactVerificationContext context){
        context.setTarget(new HttpTestTarget("localhost", 8081));
    }

    // given
    @State("ok state")
    public void stateOkState() {}

    // given
    @State("error state")
    public void stateErrorState() {}

    // when
    // then
    @TestTemplate
    @ExtendWith(PactVerificationInvocationContextProvider.class)
    void pactVerificationTestTemplate(PactVerificationContext context){
        context.verifyInteraction();
    }
}
