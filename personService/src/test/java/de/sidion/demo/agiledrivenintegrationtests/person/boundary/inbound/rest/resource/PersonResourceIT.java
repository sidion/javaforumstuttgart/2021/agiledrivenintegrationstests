package de.sidion.demo.agiledrivenintegrationtests.person.boundary.inbound.rest.resource;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class PersonResourceIT {

    @Test
    public void testPersonEndpoint() {
        given()
          .when().get("/api/person/4")
          .then()
             .statusCode(200)
             .body(is("{\"name\":\"Klaus\"}"));
    }

}
