package de.sidion.demo.agiledrivenintegrationtests.person.boundary.inbound.rest.resource;

import de.sidion.demo.agiledrivenintegrationtests.person.control.PersonController;
import de.sidion.demo.agiledrivenintegrationtests.person.entity.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class PersonResourceTest {

    @InjectMocks
    PersonResource personResource;

    @Mock
    PersonController personController;

    @Test
    public void testPersonEndpoint() {
        // given
        doReturn(Person.builder().name("Hans").build())
                .when(personController)
                .lookupPerson("1");

        // when
        Response result = personResource.getPerson(1L);

        // then
        assertThat(result.getEntity()).isEqualTo(Person.builder().name("Hans").build());
    }
}
