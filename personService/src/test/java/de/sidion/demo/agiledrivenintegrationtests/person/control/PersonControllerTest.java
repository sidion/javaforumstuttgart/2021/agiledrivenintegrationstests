package de.sidion.demo.agiledrivenintegrationtests.person.control;

import de.sidion.demo.agiledrivenintegrationtests.person.entity.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PersonControllerTest {

    @InjectMocks
    PersonController personController;

    @Mock
    PersonRepository personRepository;

    @Test
    void given_validID_when_lookupPerson_thenReturn_Person() {
        // given
        String id = "123";
        Person expectedPerson = Person.builder().name("Klaus").build();
        when(personRepository.findById(id)).thenReturn(expectedPerson);

        // when
        Person actualPerson =  personController.lookupPerson(id);

        // then)
        assertThat(actualPerson).isNotNull().isEqualTo(expectedPerson);
    }
}
