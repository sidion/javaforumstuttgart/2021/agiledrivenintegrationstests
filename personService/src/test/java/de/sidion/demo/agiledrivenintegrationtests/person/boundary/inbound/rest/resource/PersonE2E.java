package de.sidion.demo.agiledrivenintegrationtests.person.boundary.inbound.rest.resource;

import com.intuit.karate.junit5.Karate;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class PersonE2E {

    @Karate.Test
    public Karate personDataTest() {
        return new Karate().feature("classpath:karate");
    }
}
