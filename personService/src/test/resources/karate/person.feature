Feature: Personendaten abfragen

  Scenario: Daten für Person mit id 1 (Willi) abfragen
    * def personId = 1
    * def personVorname = 'Willi'
    * def personNachname = '#notpresent'

    Given url applicationBaseUrl
    And path 'api/person', personId
    When method get
    Then status 200
    And print response
    And match response.name == personVorname
    And match response.lastname == personNachname

  Scenario: Daten für Person mit id 2 (Meier) abfragen
    * def personId = 2
    * def personVorname = '#notpresent'
    * def personNachname = 'Meier'

    Given url applicationBaseUrl
    And path 'api/person', personId
    When method get
    Then status 200
    And print response
    And match response.name == personVorname
    And match response.lastname == personNachname

  Scenario: Daten für Person mit id 3 (Hans Wurst) abfragen
    * def personId = 3
    * def personVorname = 'Hans'
    * def personNachname = 'Wurst'

    Given url applicationBaseUrl
    And path 'api/person', personId
    When method get
    Then status 200
    And print response
    And match response.name == personVorname
    And match response.lastname == personNachname
