package de.sidion.demo.agiledrivenintegrationtests.person.boundary.inbound.rest.resource;

import de.sidion.demo.agiledrivenintegrationtests.person.boundary.inbound.rest.model.Message;
import de.sidion.demo.agiledrivenintegrationtests.person.control.PersonController;
import de.sidion.demo.agiledrivenintegrationtests.person.entity.Person;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("/api/person")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class PersonResource {

    @Inject
    PersonController personController;

    @GET
    @Path("{id}")
    @Parameter(
            name = "id",
            description = "ID der Person"
    )
    public Response getPerson(@PathParam("id") final Long id) {
        Person person = personController.lookupPerson(id.toString());
        if(person != null) {
            return Response.ok(person).header("abc", 123).build();
        }
        return Response.status(404)
                .entity(Message.builder().id(1000L).message("Ein Fehler ist aufgetreten").build())
                .header("xyz", 321)
                .build();
    }
}
