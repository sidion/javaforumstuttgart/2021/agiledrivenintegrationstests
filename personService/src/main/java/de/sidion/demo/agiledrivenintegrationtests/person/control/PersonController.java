package de.sidion.demo.agiledrivenintegrationtests.person.control;

import de.sidion.demo.agiledrivenintegrationtests.person.entity.Person;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PersonController {

    @Inject
    PersonRepository personRepository;

    public Person lookupPerson(String id) {
        return personRepository.findById(id);
    }
}
