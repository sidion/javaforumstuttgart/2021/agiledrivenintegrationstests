package de.sidion.demo.agiledrivenintegrationtests.person.control;

import de.sidion.demo.agiledrivenintegrationtests.person.entity.Person;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PersonRepository {

    public Person findById(String id) {
        switch (id) {
            case "1":
                return Person.builder().name("Willi").build();
            case "2":
                return Person.builder().lastname("Meier").build();
            case "3":
                return Person.builder().name("Hans").lastname("Wurst").build();
            case "9":
                return null;
            default:
                return Person.builder().name("Klaus").build();
        }
    }
}
