# Bitte die folgende Zeile in die eigene .bashrc übernehmen
#alias mvn='project_root=`git rev-parse --show-toplevel 2>/dev/null`;if [[ -e ${project_root}/.ci/build-parameter-local.sh ]]; then source ${project_root}/.ci/build-parameter-local.sh; fi; function mavenCommandWithBuildParameters(){ NEW_MVN_COMMAND="mvn ${*} ${additional_build_parameters}";echo ${NEW_MVN_COMMAND};${NEW_MVN_COMMAND}; };mavenCommandWithBuildParameters'

project_root=`git rev-parse --show-toplevel 2>/dev/null`
revision=`if [[ -e ${project_root}/version ]]; then cat ${project_root}/version; else echo 0.0.0-alpha; fi`.`date +%y%m%d.%H%M%S`
sha1=`git rev-parse --short HEAD`
branch=`git branch --show-current | cut -d'-' -f1-2`

additional_build_parameters="-Drevision=${revision} \
                             -Dsha1=${sha1} \
                             -DpactTag=${branch}"
#                             -Dpactbroker.consumerversionselectors.tags=main"
