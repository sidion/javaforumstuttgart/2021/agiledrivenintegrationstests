# agiledrivenintegrationstests

This Demo consists of 2 Microservices:
Greeting Service
Person Service


## Greeting Service

Provides a REST Endpoint (/hello/<personId>) to return a salutation for the given person ID.

This service makes use of the Person Service.


## Person Service

Provides a REST Endpoint (/api/person/<personId>) to return detailed information for the given person ID.


## Pactbroker

To start the pactbroker open a commandshel in packbroker subdirectory and execute the following command:

`docker-compose up`

The broker will be available at http://localhost:9292/


