package de.sidion.demo.agiledrivenintegrationtests.greeting.control;

import de.sidion.demo.agiledrivenintegrationtests.greeting.boundary.outbound.rest.client.PersonServiceClient;
import de.sidion.demo.agiledrivenintegrationtests.greeting.entity.Person;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class GreetingController {

    @Inject @RestClient
    PersonServiceClient personServiceClient;

    public String getPerson(String id) {

        String greeting = "Hello my unknown friend";
        Person person = personServiceClient.getPerson(id);
        if(person != null) {
            greeting = "Hello" + person.getNameDisplayname() + person.getLastnameDisplayname();
        }
        return greeting;
    }
}
