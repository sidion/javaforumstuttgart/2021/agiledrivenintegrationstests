package de.sidion.demo.agiledrivenintegrationtests.greeting.boundary.inbound.rest.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    Long id;
    String message;
}
