package de.sidion.demo.agiledrivenintegrationtests.greeting.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    private String name;
    private String lastname;

    public String getNameDisplayname(){
        return name != null ? " ".concat(name) : "";
    }

    public String getLastnameDisplayname(){
        return lastname != null ? " ".concat(lastname) : "";
    }
}
