package de.sidion.demo.agiledrivenintegrationtests.greeting.boundary.outbound.rest.client;

import de.sidion.demo.agiledrivenintegrationtests.greeting.boundary.inbound.rest.model.Message;
import de.sidion.demo.agiledrivenintegrationtests.greeting.entity.Person;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@ApplicationScoped
@RestClient
public class PersonServiceClient {

    public static final String API_PERSON_URL = "/api/person/";

    private final ExecutorService executorService = Executors.newCachedThreadPool();
    private final Client client;

    @ConfigProperty(name = "person.resource.baseUrl")
    String baseUrl;

    @Inject
    public PersonServiceClient() {
        client = ClientBuilder.newBuilder().executorService(executorService).build();
    }

    public Person getPerson(final String id) {

        final Response response = client.target(baseUrl + API_PERSON_URL + id)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
        if(response.getStatus() == 200){
            Object o = response.getEntity();
            Person person = null;
            try {
                person = response.readEntity(Person.class);
            } catch(Throwable t) {
                System.out.println(t.getMessage());
            } finally {
                response.close();
            }
            return person;
        }
        return null;
    }
}
