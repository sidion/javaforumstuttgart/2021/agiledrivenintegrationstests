package de.sidion.demo.agiledrivenintegrationtests.greeting.boundary.inbound.rest.resource;

import de.sidion.demo.agiledrivenintegrationtests.greeting.control.GreetingController;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@ApplicationScoped
@Path("/hello")
public class GreetingResource {

    @Inject
    GreetingController greetingController;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("{id}")
    public String hello(@PathParam("id") final String id) {
        return greetingController.getPerson(id);
    }
}
