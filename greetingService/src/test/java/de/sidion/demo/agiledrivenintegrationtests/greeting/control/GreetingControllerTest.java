package de.sidion.demo.agiledrivenintegrationtests.greeting.control;

import de.sidion.demo.agiledrivenintegrationtests.greeting.boundary.outbound.rest.client.PersonServiceClient;
import de.sidion.demo.agiledrivenintegrationtests.greeting.entity.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class GreetingControllerTest {

    @InjectMocks
    GreetingController greetingController;

    @Mock
    PersonServiceClient personServiceClient;

    @Test
    void given_validID_when_sayHello_thenReturn_personalizedHello() {
        // given
        String id = "123";
        Person klaus = Person.builder().name("Klaus").build();
        doReturn(klaus).when(personServiceClient).getPerson(id);

        // when
        String salutation =  greetingController.getPerson(id);

        // then)
        assertThat(salutation).isNotNull().isEqualTo("Hello Klaus");
    }
}
