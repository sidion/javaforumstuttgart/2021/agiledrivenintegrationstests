package de.sidion.demo.agiledrivenintegrationtests.greeting.boundary.outbound.rest.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;

import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class PersonMockTestResource implements QuarkusTestResourceLifecycleManager {

    private WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {

        // json files werden durch PACT Tests generiert und unter target/test-classes/__files abgelegt
        wireMockServer = new WireMockServer(options().port(8090).withRootDirectory("target/test-classes/"));
        wireMockServer.start();
        configureFor(wireMockServer.port());

        stubFor(get(urlEqualTo("/api/person/1"))
            .willReturn(aResponse()
                .withHeader("Content-Type", "application/json")
                .withStatus(Response.Status.OK.getStatusCode())
                .withBodyFile("personMockTestResource/1.json")));

        stubFor(get(urlEqualTo("/api/person/2"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(Response.Status.OK.getStatusCode())
                        .withBodyFile("personMockTestResource/2.json")));

        stubFor(get(urlEqualTo("/api/person/3"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(Response.Status.OK.getStatusCode())
                        .withBodyFile("personMockTestResource/3.json")));

        stubFor(get(urlEqualTo("/api/person/9"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(Response.Status.NOT_FOUND.getStatusCode())
                        .withBodyFile("personMockTestResource/9.json")));

        return Collections.singletonMap("person.resource.baseUrl", wireMockServer.baseUrl());
    }

    @Override
    public void stop() {

    }
}
