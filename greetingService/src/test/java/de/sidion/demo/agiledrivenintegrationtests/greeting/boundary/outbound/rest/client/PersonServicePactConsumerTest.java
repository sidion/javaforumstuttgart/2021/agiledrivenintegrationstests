package de.sidion.demo.agiledrivenintegrationtests.greeting.boundary.outbound.rest.client;

import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslJsonBody;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.core.model.RequestResponsePact;
import au.com.dius.pact.core.model.annotations.Pact;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "PersonProvider")
public class PersonServicePactConsumerTest {

    //private Map<String,String> headers = Map.of("Content-Type", "application/json");
    private Map<String,String> headers = MapUtils.putAll(new HashMap<>(),new String[] {
            "Content-Type", "application/json"
    });

    @BeforeAll
    public static void setupClass() throws IOException {
        // Create Target directories for the Wiremock json files
        if(Files.notExists(Paths.get("target/test-classes/__files/"))) {
            Files.createDirectory(Paths.get("target/test-classes/__files/"));
        }
        if(Files.notExists(Paths.get("target/test-classes/__files/personMockTestResource"))) {
            Files.createDirectory(Paths.get("target/test-classes/__files/personMockTestResource"));
        }
    }

    @BeforeEach
    public void setUp(MockServer mockServer) {
        assertThat(mockServer).isNotNull();

    }

    @Pact(provider="PersonProvider", consumer="person_consumer")
    public RequestResponsePact createPact4Willi(PactDslWithProvider builder) throws IOException {
        PactDslJsonBody body = new PactDslJsonBody()
                .asBody()
                .stringValue("name","Willi");
        // persistiere body für den wiremock server für weitere Integrationstests
        Path myNewFilePath = Paths.get("target/test-classes/__files/personMockTestResource/1.json");
        Files.writeString(myNewFilePath, body.toString());
        return builder
                .given("ok state")
                .uponReceiving("Example request #1 for Willi, no lastname")
                .path("/api/person/1")
                .method("GET")
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body(body)
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "createPact4Willi")
    void testWilli(MockServer mockServer) throws IOException {
        HttpResponse httpResponse = Request.Get(mockServer.getUrl() + "/api/person/1")
                .execute()
                .returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(200);
        assertThat(IOUtils.toString(httpResponse.getEntity().getContent())).isEqualTo("{\"name\":\"Willi\"}");
    }

    @Pact(provider="PersonProvider", consumer="person_consumer")
    public RequestResponsePact createPact4Meier(PactDslWithProvider builder) throws IOException {
        PactDslJsonBody body = new PactDslJsonBody()
                .asBody()
                .stringValue("lastname","Meier");
        // persistiere body für den wiremock server für weitere Integrationstests
        Path myNewFilePath = Paths.get("target/test-classes/__files/personMockTestResource/2.json");
        Files.writeString(myNewFilePath, body.toString());
        return builder
                .given("ok state")
                .uponReceiving("Example request #2 for Meier, no firstname")
                .path("/api/person/2")
                .method("GET")
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body(body)
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "createPact4Meier")
    void testMeier(MockServer mockServer) throws IOException {
        HttpResponse httpResponse = Request.Get(mockServer.getUrl() + "/api/person/2")
                .execute()
                .returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(200);
        assertThat(IOUtils.toString(httpResponse.getEntity().getContent())).isEqualTo("{\"lastname\":\"Meier\"}");
    }

    @Pact(provider="PersonProvider", consumer="person_consumer")
    public RequestResponsePact createPact4HansWurst(PactDslWithProvider builder) throws IOException {
        PactDslJsonBody body = new PactDslJsonBody()
                .asBody()
                .stringValue("name","Hans")
                .stringValue("lastname","Wurst");
        // persistiere body für den wiremock server für weitere Integrationstests
        Path myNewFilePath = Paths.get("target/test-classes/__files/personMockTestResource/3.json");
        Files.writeString(myNewFilePath, body.toString());
        return builder
                .given("ok state")
                .uponReceiving("Example request #3 for Hans Wurst")
                .path("/api/person/3")
                .method("GET")
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body(body)
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "createPact4HansWurst")
    void testHansWurst(MockServer mockServer) throws IOException {
        HttpResponse httpResponse = Request.Get(mockServer.getUrl() + "/api/person/3")
                .execute()
                .returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(200);
        assertThat(IOUtils.toString(httpResponse.getEntity().getContent())).isEqualTo("{\"lastname\":\"Wurst\",\"name\":\"Hans\"}");
    }

    @Pact(provider="PersonProvider", consumer="person_consumer")
    public RequestResponsePact createPact4UnknownUser(PactDslWithProvider builder) throws IOException {
        PactDslJsonBody body = new PactDslJsonBody()
                .asBody()
                .integerType("id", 123)
                .stringType("message","Something unexpected happened");
        // persistiere body für den wiremock server für weitere Integrationstests
        Path myNewFilePath = Paths.get("target/test-classes/__files/personMockTestResource/9.json");
        Files.writeString(myNewFilePath, body.toString());
        return builder
                .given("error state")
                .uponReceiving("Example request #9 for Unknown Person")
                .path("/api/person/9")
                .method("GET")
                .willRespondWith()
                .status(404)
                .headers(headers)
                .body(body)
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "createPact4UnknownUser")
    void testnknownUser(MockServer mockServer) throws IOException {
        HttpResponse httpResponse = Request.Get(mockServer.getUrl() + "/api/person/9")
                .execute()
                .returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode()).isEqualTo(404);
    }
}
