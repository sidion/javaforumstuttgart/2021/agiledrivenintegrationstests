package de.sidion.demo.agiledrivenintegrationtests.greeting.boundary.inbound.rest.resource;

import de.sidion.demo.agiledrivenintegrationtests.greeting.control.GreetingController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class GreetingResourceTest {

    @InjectMocks
    GreetingResource greetingResource;

    @Mock
    GreetingController greetingController;

    @Test
    public void testHelloEndpoint() {
        // given
        doReturn("hallo Kunigunde")
                .when(greetingController)
                .getPerson("987");

        // when
        String result = greetingResource.hello("987");

        // then
        assertThat(result).isEqualTo("hallo Kunigunde");
    }
}
