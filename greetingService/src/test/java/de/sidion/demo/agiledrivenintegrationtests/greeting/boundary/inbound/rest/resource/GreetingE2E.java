package de.sidion.demo.agiledrivenintegrationtests.greeting.boundary.inbound.rest.resource;

import com.intuit.karate.junit5.Karate;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class GreetingE2E {

    @Karate.Test
    public Karate greetingTest() {
        return new Karate().feature("classpath:karate");
    }
}
