package de.sidion.demo.agiledrivenintegrationtests.greeting.boundary.inbound.rest.resource;

import de.sidion.demo.agiledrivenintegrationtests.greeting.boundary.outbound.rest.resource.PersonMockTestResource;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@QuarkusTestResource(PersonMockTestResource.class)
public class GreetingResourceIT {

    @Test
    public void given_hello1_when_get_then_helloWilli() {
        given()
          .when().get("/hello/1")
          .then()
             .statusCode(200)
             .body(is("Hello Willi"));
    }

    @Test
    public void given_hello9_when_get_then_error() {
        given()
                .when().get("/hello/9")
                .then()
                .statusCode(200)
                .body(is("Hello my unknown friend"));
    }
}
