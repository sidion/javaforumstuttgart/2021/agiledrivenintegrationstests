function fn() {
    karate.configure('logPrettyRequest', true);
    karate.configure('logPrettyResponse', true);
    karate.configure('connectTimeout', 30000);
    karate.configure('readTimeout', 30000);

    var applicationBaseUrl = karate.properties['application.baseUrl'];
    if(!applicationBaseUrl) {
        applicationBaseUrl = 'http://localhost:8081';
    }

    var config = {
        applicationBaseUrl : applicationBaseUrl
    };

    return config;
}
