Feature: Personen begrüssen

  Scenario: Willi begrüssen, der nur mit Vornamen bekannt ist
    * def personId = 1
    * def personVorname = 'Willi'

    Given url applicationBaseUrl
    And path 'hello', personId
    When method get
    Then status 200
    And print response
    And match response == 'Hello ' + personVorname

  Scenario: Meier begrüssen, der nur mit Nachnamen bekannt ist
    * def personId = 2
    * def personNachname = 'Meier'

    Given url applicationBaseUrl
    And path 'hello', personId
    When method get
    Then status 200
    And print response
    And match response == 'Hello ' + personNachname

  Scenario: Hans Wurst begrüssen, der mit vollem Namen bekannt ist
    * def personId = 3
    * def personVorname = 'Hans'
    * def personNachname = 'Wurst'

    Given url applicationBaseUrl
    And path 'hello', personId
    When method get
    Then status 200
    And print response
    And match response == 'Hello ' + personVorname + ' ' + personNachname

  Scenario: Fehlerfall, wenn nach eine unbekannte Person begrüßt werden soll
    * def personId = 9
    * def message = 'Hello my unknown friend'

    Given url applicationBaseUrl
    And path 'hello', personId
    When method get
    Then status 200
    And print response
    And match response == message
